from zkp import CA
import asyncio
import websockets


async def serve(websocket, path):
    ca = CA()
    hello_message = "Отправь мне свой индентификатор!"
    await websocket.send(hello_message)
    ID = await websocket.recv()
	# конвертируем обратно в число
    ID = int(ID)
    S_A = ca.sqrtmod(ID)
    S_A = str(S_A)


    print(f"Отправляю A его секрет S_A: {S_A}")
    await websocket.send(S_A)

    print(f"Отправляю A мое составное число N: {ca.N}")
    await websocket.send(str(ca.N))


print("CA started!")
start_server = websockets.serve(serve, "localhost", 1234)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
