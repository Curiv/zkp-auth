from zkp import User
import random
import asyncio
import websockets


async def communicate(uris, user):
    uri_ca = uris["ca"]
    uri_b  = uris["b"]

    async with websockets.connect(uri_ca) as websocket:
        welcome_message  = await websocket.recv()
        print(f"CA сообщает: {welcome_message}")
		# данные из вебсокетов необходимо конвертировать в строку перед отправкой
        user.id = str(user.id)
        print(f"Отправляю CA свой идентификатор: {user.id}")
        await websocket.send(user.id)
        secret  = await websocket.recv()
        print(f"Получен S_A от CA: {secret}")
        N = await websocket.recv()
        N = int(N)

        try:
            A.S = int(secret)
        except ValueError as e:
            print(e, "Выберите другой индентификатор!")
            exit()

        A.gen_gamma(A.r, N)
        A.gen_z(A.S, A.r, N)

    async with websockets.connect(uri_b) as websocket:
        print(f"Отправляю B свое число γ: {A.gamma}")
        await websocket.send(str(A.gamma))

        print(f"Отправляю B свое число z: {A.z}")
        await websocket.send(str(A.z))


# словарь с адресами для подключений к CA и к B
uris = {"ca":"ws://localhost:1234", "b":"ws://localhost:4321"}

# инициализация параметров пользователя А
A = User()
A.id = 100
A.r = random.randint(1, 2 ** 16)

asyncio.get_event_loop().run_until_complete(
    communicate(uris, A)
)

