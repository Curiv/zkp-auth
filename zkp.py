import libnum


# функция возведения в степень по модулю N
def expmod_2(number, N):
    return pow(number, 2) % N

# удостоверяющий центр
class CA:
    # инициализация класса простыми множителями
    def __init__(self):
        self.p = 8825211809755757906571799039777021165310709572635773163079784082245490367642738349038773888738119251715720511931952779965338500765564082641346324795914539
        self.q = 7888324920534900225546479282649263205707931887050873336461752544352415205499148254824381298192176506414764221748931815951112397508239020855651223000334831
        # составное число N=p*q
        self.N = self.p * self.q

	# функция взятия квадратного корня по модулю N
    def sqrtmod(self, ID):
        try:
            # создание словаря с известными простыми множителями
            factors = {self.p:1, self.q:1}
            reminders = list(libnum.sqrtmod(ID, factors))
            reminders.sort()
            return reminders.pop()
        except ValueError as e:
            print(e)
            return None


# класс для пользователя
class User:
    def __init__(self):
        # инициализация ID пользователя
        self.id = 101
        self.S  = 0
        self.r  = 50

    # функция возведения в степень по модулю N
    def expmod_2(self, number, N):
        return pow(number, 2) % N

    # функция генерации числа Гамма
    def gen_gamma(self, r, N):
        self.gamma = expmod_2(r, N)
        return self.gamma

    # генерация числа z
    def gen_z(self, S, r, N):
        self.z = (S*r) % N
        return self.z

    # генерация числа u
    def gen_u(self, z, N):
        self.u = expmod_2(z, N)
        return self.u

    # генерация числа w
    def gen_w(self, u, gamma, N):
        self.w = (self.u * libnum.invmod(gamma, N)) % N
        return self.w
