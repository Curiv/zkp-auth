from zkp import User
import asyncio
import websockets


async def serve(websocket, path):
    B = User()
    gamma_A  = await websocket.recv()
    gamma_A = int(gamma_A)
    print(f"Получено число γ от A: {gamma_A}")

    z_A  = await websocket.recv()
    z_A = int(z_A)
    print(f"Получено число z от A: {z_A}")
    
    N = 69616138247895251995431161652999064073522124752294359718459844906217282165857728927175108530324613383259909916525011029432215900315122414457446256596463501041176575898001381179579876840376647914137477900091005017494366009206509828215884595343913409518687489691906141087883442808176070646165330108937861007909
    B.gen_u(z_A, N)
    B.gen_w(B.u, gamma_A, N)
    print(f"Полученный идентификатор пользователя A: {B.w}")


print("Пользователь B запущен")
start_server = websockets.serve(serve, "localhost", 4321)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

